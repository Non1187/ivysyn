#!/bin/bash
#
# This script runs IvySyn on either PyTorch or TensorFlow
#

if [[ -z "${IVYSYN_PATH}" ]]; then
    echo "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!"
    exit 1
fi

if [[ -z "${IVYSYN_TMP_RESULTS_PATH}" ]]; then
    echo -n "You need to set the IVYSYN_TMP_RESULTS_PATH environment variable to the directory "
    echo "you want IvySyn to temporarily output results while fuzzing!";
    exit 1
fi

IVYSYN_SCRIPTS_BASE="${IVYSYN_PATH}/src/ivysyn/scripts/"
VENV_BASE="${IVYSYN_PATH}/venv/"
FRAMEWORKS_BASE="${IVYSYN_PATH}/src/frameworks/"

TENSORFLOW_PATH="${FRAMEWORKS_BASE}/tensorflow-2.6-ivysyn/"
TENSORFLOW_PYTHON_PATH="${TENSORFLOW_PATH}/tensorflow/python/"
TENSORFLOW_SCRIPTS_PATH="${IVYSYN_PATH}/src/ivysyn/tensorflow/scripts/"
TENSORFLOW_RESULTS_PATH="${IVYSYN_PATH}/results/tensorflow/"
TENSORFLOW_CRASHES_PATH="${TENSORFLOW_RESULTS_PATH}/crashes/"

PYTORCH_PATH="${FRAMEWORKS_BASE}/pytorch-1.11-ivysyn/"
PYTORCH_SCRIPTS_PATH="${IVYSYN_PATH}/src/ivysyn/pytorch/scripts/"
PYTORCH_RESULTS_PATH="${IVYSYN_PATH}/results/pytorch/"
PYTORCH_CRASHES_PATH="${PYTORCH_RESULTS_PATH}/crashes/"
VENV_BASE="${IVYSYN_PATH}/venv/"
CONDA_ACTIVATE_PATH="${VENV_BASE}anaconda3/bin/activate"

DO_TF_RUN=0
DO_PT_RUN=0

crash_subdirs=("segfault" "fpe" "abort" "other")

do_tf_run()
{
    # Reset RNG seeds in python scripts
    pushd "${TENSORFLOW_PYTHON_PATH}"
    git restore *
    popd

    pushd "${TENSORFLOW_SCRIPTS_PATH}"

    # Set the RNG seeds in the tests
    python3 make_tests_deterministic.py

    rm "${IVYSYN_PATH}/src/ivysyn/tensorflow/raw_regs.txt"
    source "${VENV_BASE}tensorflow-2.6-ivysyn/bin/activate"

    # Run the developer-provided framework tests
    echo "Running developer tests"
    if [[ -z "${NUM_EXPECTED_KERNELS}" ]]; then
        python run_kernel_tests.py
    else
        python run_kernel_tests.py --nkernels "${NUM_EXPECTED_KERNELS}"
    fi

    suffix=$(head -c 100 /dev/urandom | md5sum | cut -d ' ' -f 1)
    out_dir_name="run${ffuzzer}_${suffix}"
    out_dir="${TENSORFLOW_CRASHES_PATH}/${out_dir_name}"

    # Move the results to the proper directory
    echo "Moving results"
    mkdir -p "${out_dir}"
    mv "${IVYSYN_TMP_RESULTS_PATH}/"* "${out_dir}"

    # Run the synthesizer and check PoV reproducibility
    echo "Running synthesizer and checking reproducibility"
    python3 synthesizer.py --dir "${out_dir_name}"
    deactivate

    ./reproducer.sh --dir "${out_dir_name}"
    popd

    # Copy over the reproducible PoVs
    echo "Collecting the PoVs"
    pushd "${IVYSYN_SCRIPTS_BASE}"
    python3 collect_povs.py --dir "${out_dir_name}" --tensorflow
    python3 summarize_run.py --dir "${out_dir_name}" --tensorflow
    popd

    if [[ -n "${fuzzer}" ]]; then
        pushd "${TENSORFLOW_SCRIPTS_PATH}/${fuzzer}_comp_scripts"
        python3 parse_comp_results.py
        popd
    fi

}

do_pt_run()
{
    source "${CONDA_ACTIVATE_PATH}"
    conda activate pytorch-1.11-ivysyn

    pushd "${PYTORCH_SCRIPTS_PATH}"

    # Run the developer-provided framework tests
    echo "Running developer tests"
    if [[ -z "${NUM_EXPECTED_KERNELS}" ]]; then
        python run_kernel_tests.py
    else
        python run_kernel_tests.py --nkernels "${NUM_EXPECTED_KERNELS}"
    fi

    suffix=$(head -c 100 /dev/urandom | md5sum | cut -d ' ' -f 1)
    out_dir_name="run${ffuzzer}_${suffix}"
    out_dir="${PYTORCH_CRASHES_PATH}/${out_dir_name}"

    # Move the results to the proper directory
    echo "Moving results"
    mkdir -p "${out_dir}"
    mv "${IVYSYN_TMP_RESULTS_PATH}/"* "${out_dir}"

    # Run the synthesizer and check PoV reproducibility
    echo "Running synthesizer and checking reproducibility"
    python3 synthesizer.py --dir "${out_dir_name}"
    conda deactivate

    ./reproducer.sh --dir "${out_dir_name}"
    popd

    # Copy over the reproducible PoVs
    echo "Collecting the PoVs"
    pushd "${IVYSYN_SCRIPTS_BASE}"
    python3 collect_povs.py --dir "${out_dir_name}" --pytorch
    python3 summarize_run.py --dir "${out_dir_name}" --pytorch
    popd

    if [[ -n "${fuzzer}" ]]; then
        pushd "${PYTORCH_SCRIPTS_PATH}/${fuzzer}_comp_scripts"
        python3 parse_comp_results.py
        popd
    fi
}

do_usage()
{
    echo "Usage: ./`basename $0` --seed <rngseed> --tensorflow|--pytorch [--docter|--atheris] [--nkernels NUM_EXPECTED_KERNELS]"
    echo "--seed <rngseed>: provide an integer as the seed to be used by IvySyn's Random Number Generators"
    echo "--tesnorflow|--pytorch: DL framework to run on (must select ONE)"
    echo "--docter|atheris: [Optional] set if this run will be used for a comparison with another fuzzer (Atheris or DocTer)"
    echo "--nkernels NUM_EXPECTED_KERNELS: [Optional] provide an integer to force IvySyn to stop fuzzing early, when NUM_EXPECTED_KERNELS kernels have been fuzzed"
    exit 1
}

main()
{
    fuzzer=""
    ffuzzer=""

    POSITIONAL=()
    while [[ $# -gt 0 ]]; do

        key="$1"

        case $key in
        --tensorflow)
            shift
            DO_TF_RUN=1
            ;;
        --pytorch)
            shift
            DO_PT_RUN=1
            ;;
        --docter)
            shift
            fuzzer="docter"
            ffuzzer="_docter"
            ;;
        --atheris)
            shift
            fuzzer="atheris"
            ffuzzer="_atheris"
            ;;
        --seed)
            shift
            RNGSEED="$1"
            shift
            ;;
        --nkernels)
            shift
            NUM_EXPECTED_KERNELS="$1"
            shift
            ;;
        *)
            echo "Unknown option: $1"
            do_usage
            ;;
        esac

    done

    set -- "${POSITIONAL[@]}"

    if [[ -z "${RNGSEED}" ]]; then
        do_usage
    fi

    rm "${IVYSYN_TMP_RESULTS_PATH}"/*

    echo "${RNGSEED}" > /tmp/ivy-seed

    if [[ "${DO_TF_RUN}" -eq 1 && "${DO_PT_RUN}" -eq 1 ]] ||
        [[ "${DO_TF_RUN}" -eq 0 && "${DO_PT_RUN}" -eq 0  ]]; then
        echo "Select ONE of --tensorflow or --pytorch"
        do_usage
    fi

    if [[ "${DO_TF_RUN}" -eq 1 ]]; then
        do_tf_run
    fi

    if [[ "${DO_PT_RUN}" -eq 1 ]]; then
        do_pt_run
    fi
}

main $@
