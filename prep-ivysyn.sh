#!/bin/bash

# This script prepares an IvySyn instance:
# - Mounts the directories where the IvySyn fuzzer will spit out logs as a tmpfs
# - Installs framework dependencies and virtual environments
# - Builds and installs PyTorch and TensorFlow

if [[ -z "${IVYSYN_PATH}" ]]; then
    echo "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!";
    exit 1
fi

IVYSYN_SCRIPTS_BASE="${IVYSYN_PATH}/src/ivysyn/scripts/"
IVYSYN_RESULTS_DIR="${IVYSYN_PATH}/results/"
FRAMEWORKS_BASE="${IVYSYN_PATH}/src/frameworks/"
VENV_BASE="${IVYSYN_PATH}/venv/"
PYTORCH_BASE="${FRAMEWORKS_BASE}pytorch-1.11-ivysyn/"
TENSORFLOW_BASE="${FRAMEWORKS_BASE}tensorflow-2.6-ivysyn/"
CONDA_ACTIVATE_PATH="${VENV_BASE}anaconda3/bin/activate"

bazel_oom_message()
{
    echo "TensorFlow build failed. Your system probably run out of memory while building." >&2
    echo -n "Try reducing the number of parallel jobs used by bazel by changing the value " >&2
    echo -n " of the '-j' argument for the 'bazel build ... ' command in `basename $0`" >&2
    echo " (e.g., bazel build -j 2 ...)." >&2
    echo "If the problem persists, contact the authors." >&2

    exit 1
}

mount_result_dirs()
{
    pushd "${IVYSYN_SCRIPTS_BASE}"
    bash mount-result-dirs.sh
    popd
}

install_anaconda()
{
    pushd "${VENV_BASE}"
    wget https://repo.anaconda.com/archive/Anaconda3-2022.05-Linux-x86_64.sh -O "${VENV_BASE}/Anaconda3-2022.05-Linux-x86_64.sh"
    bash Anaconda3-2022.05-Linux-x86_64.sh -b -p "${VENV_BASE}anaconda3"
    popd
}

install_pytorch()
{
    # Clone PyTorch repo
    git clone --depth 1 -b v1.11.0 --recursive https://github.com/pytorch/pytorch.git ${PYTORCH_BASE}
    pushd ${PYTORCH_BASE}
    git submodule sync
    git submodule update --init --recursive
    popd

    source "${CONDA_ACTIVATE_PATH}"

    # Install pre-built PyTorch version 1.11 from PyPI with anaconda
    # (will be used to check PoV reproducibility)
    conda create --yes --name pytorch-1.11-orig
    conda activate pytorch-1.11-orig
    conda install --yes pytorch=1.11 torchvision torchaudio cpuonly -c pytorch
    conda deactivate

    # Install pre-built latest PyTorch version with anaconda
    conda create --yes --name pytorch-1.12-orig
    conda activate pytorch-1.12-orig
    conda install --yes pytorch torchvision torchaudio cpuonly -c pytorch
    conda deactivate

    # Create IvySyn virtual environment for our PyTorch installation and install dependencies
    conda create --name pytorch-1.11-ivysyn --yes --file ${IVYSYN_PATH}/pytorch-conda-requirements.txt
    conda activate pytorch-1.11-ivysyn
    python3 -m pip install -r ${IVYSYN_PATH}/pytorch-pip-requirements.txt

    # Install PyTorch
    export CMAKE_PREFIX_PATH=${CONDA_PREFIX:-"$(dirname $(which conda))/../"}
    pushd ${PYTORCH_BASE}
    python3 setup.py install
    popd

    # This script will clone the right version of PyTorch, apply the necessary patches
    # to make the framework compatible with IvySyn and perform the kernel instrumentation
    # to inject the IvySyn wrappers
    pushd ${IVYSYN_SCRIPTS_BASE}
    bash prep-pytorch-ivysyn.sh
    popd

    pushd ${PYTORCH_BASE}
    python3 setup.py clean
    python3 setup.py develop
    python3 setup.py install
    popd

    conda deactivate

}

install_tensorflow()
{
    # Download bazel
    wget https://github.com/bazelbuild/bazelisk/releases/download/v1.11.0/bazelisk-linux-amd64
    [[ -d ${HOME}/.local/bin ]] || mkdir -p ${HOME}/.local/bin
    mv bazelisk-linux-amd64 ${HOME}/.local/bin/bazel
    chmod +x ${HOME}/.local/bin/bazel
    PATH=${PATH}:${HOME}/.local/bin

    # Clone TensorFlow
    git clone --depth 1 -b v2.6.0 --recursive https://github.com/tensorflow/tensorflow.git ${TENSORFLOW_BASE}

    # Create virtual environments and install the pre-build TensorFlow binaries from PyPI
    # (will be used to check PoV reproducibility)
    pushd ${VENV_BASE}
    python3 -m venv tensorflow-2.6-ivysyn
    python3 -m venv tensorflow-2.6-orig
    python3 -m venv tensorflow-2.9-orig

    # Install dependencies
    source "${VENV_BASE}tensorflow-2.6-orig/bin/activate"
    pip3 install tensorflow==2.6.0
    python3 -m pip install protobuf==3.20.*
    deactivate

    source "${VENV_BASE}tensorflow-2.9-orig/bin/activate"
    pip3 install tensorflow==2.9
    deactivate

    popd

    # This script will clone the right version of TensorFlow, apply the necessary patches
    # to make the framework compatible with IvySyn and perform the kernel instrumentation
    # to inject the IvySyn wrappers
    pushd ${IVYSYN_SCRIPTS_BASE}
    bash prep-tensorflow-ivysyn.sh
    popd

    # Build and install TensorFlow
    pushd ${TENSORFLOW_BASE}
    source "${VENV_BASE}tensorflow-2.6-ivysyn/bin/activate"
    pip3 install -U pip numpy wheel packaging
    pip3 install -U keras_preprocessing --no-deps
    pip3 install -r ${IVYSYN_PATH}/tensorflow-requirements.txt
    PYTHON_BIN_PATH="${VENV_BASE}tensorflow-2.6-ivysyn/bin/python3" USE_DEFAULT_PYTHON_LIB_PATH=1 \
        TF_NEED_CUDA=0 TF_NEED_ROCM=0 TF_DOWNLOAD_CLANG=0 CC_OPT_FLAGS=0 TF_SET_ANDROID_WORKSPACE=0 \
        ./configure
    bazel build //tensorflow/tools/pip_package:build_pip_package --runs_per_test=10 --cache_test_results=no
    if [[ ! "$?" -eq 0 ]]; then
        bazel_oom_message
    fi
    bazel build //tensorflow/core/kernels:all --runs_per_test=10 --cache_test_results=no
    if [[ ! "$?" -eq 0 ]]; then
        bazel_oom_message
    fi
    ./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp
    pip3 install --force-reinstall /tmp/tensorflow-2.6.0-cp39-cp39-linux_x86_64.whl
    pip3 install -r ${IVYSYN_PATH}/tensorflow-requirements.txt
    popd

    # Collects the kernel-to-binding mappings which will be used by other
    # IvySyn scripts
    pushd "${IVYSYN_PATH}/src/ivysyn/tensorflow/scripts"
    python3 collect_registered_kernels.py
    popd

}

main()
{
    # Mount fuzzing result directories
    mount_result_dirs

    # Install anaconda3 for PyTorch virtual environments
    install_anaconda

    # Download, patch and install the frameworks
    install_pytorch
    install_tensorflow
}

main
