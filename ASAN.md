# Instructions for Compiling with ASan

Here, we provide instructions about how to compile either framework with [AddressSanitizer](https://github.com/google/sanitizers/wiki/AddressSanitizer) (ASan), in case you wish to analyze the memory errors found by `IvySyn`.

## Pytorch

### Installation

First, restore the modified kernels by running the following:

    cd ${IVYSYN_PATH}/src/frameworks/pytorch-1.11-ivysyn/aten/src/ATen/native
    rm native_functions_no_dups.yaml
    git restore *

Next, create a new virtual environment:

    conda create --name pytorch-1.11-asan
    conda activate pytorch-1.11-asan

If you have installed PyTorch before (e.g., by following the instructions in the [README](README.md)), make sure to clean the old installation first by running `python3 setup.py clean` in the PyTorch root directory.

    cd ${IVYSYN_PATH}/src/frameworks/pytorch-1.11-ivysyn
    python3 setup.py clean # If PyTorch has been compiled before
    export CMAKE_PREFIX_PATH=${CONDA_PREFIX:-"$(dirname $(which conda))/../"}
    ASAN_OPTIONS=detect_leaks=0:symbolize=1:detect_odr_violation=0 CC="clang" CXX="clang++" LDSHARED="clang --shared" CFLAGS="-fsanitize=address -Wno-unused-command-line-argument -shared-libasan -pthread -Wl,-rpath=$(dirname $($CXX --print-file-name libclang_rt.asan-x86_64.so)) -Wno-unused-command-line-argument -fno-omit-frame-pointer" CXX_FLAGS="-fsanitize=address -shared-libasan -pthread -Wl,-rpath=$(dirname $($CXX --print-file-name libclang_rt.asan-x86_64.so)) -fno-omit-frame-pointer" USE_ASAN=1 DEBUG=0 python3 setup.py develop
    ASAN_OPTIONS=detect_leaks=0:symbolize=1:detect_odr_violation=0 CC="clang" CXX="clang++" LDSHARED="clang --shared" CFLAGS="-fsanitize=address -Wno-unused-command-line-argument -shared-libasan -pthread -Wl,-rpath=$(dirname $($CXX --print-file-name libclang_rt.asan-x86_64.so)) -Wno-unused-command-line-argument -fno-omit-frame-pointer" CXX_FLAGS="-fsanitize=address -shared-libasan -pthread -Wl,-rpath=$(dirname $($CXX --print-file-name libclang_rt.asan-x86_64.so)) -fno-omit-frame-pointer" USE_ASAN=1 DEBUG=0 python3 setup.py install

### Running PoVs with ASan

To run a PoV with the ASan-compiled PyTorch installation, first activate the [Conda](https://docs.conda.io/en/latest/) virtual environment containing the ASan installation, and then run the PoV with the environment variables set as follows:

    conda activate pytorch-1.11-asan
    LD_PRELOAD=$(clang --print-file-name libclang_rt.asan-x86_64.so) ASAN_OPTIONS=detect_leaks=0:symbolize=1:detect_odr_violation=0 python3 pov.py

## TensorFlow

### Installation

First, restore the modifications made by `IvySyn` by running the following:

    cd ${IVYSYN_PATH}/src/frameworks/tensorflow-2.6-ivysyn/tensorflow/core/kernels
    git restore *
    cd ${IVYSYN_PATH}/src/frameworks/tensorflow-2.6-ivysyn/python
    git restore *

If you have previously installed TensorFlow from source (e.g., by following the instructions in the [README](README.md)), clean the build by running:

    cd ${IVYSYN_PATH}/frameworks/tensorflow-2.6-ivysyn
    bazel clean --expunge

Next, create new virtual environment and install the following dependencies:

    cd ${IVYSYN_PATH}/venv
    python3 -m venv tensorflow-2.6-asan
    source ${IVYSYN_PATH}/venv/tensorflow-2.6-asan/bin/activate
    pip3 install -U pip numpy wheel packaging
    pip3 install -U keras_preprocessing --no-deps
    pip3 install -r ${IVYSYN_PATH}/tensorflow-requirements.txt

Next, apply the following patches:

    patch ${IVYSYN_PATH}/src/frameworks/tensorflow-2.6-ivysyn/third_party/jpeg/BUILD.bazel ${IVYSYN_PATH}/src/ivysyn/tensorflow/patches/jpeg_BUILD.bazel.patch
    patch ${IVYSYN_PATH}/src/frameworks/tensorflow-2.6-ivysyn/.bazelrc ${IVYSYN_PATH}/src/ivysyn/tensorflow/patches/bazelrc.patch

Finally, build and install TensorFlow with ASan:

    cd ${IVYSYN_PATH}/src/frameworks/tensorflow-2.6-ivysyn
    source ${IVYSYN_PATH}/venv/tensorflow-2.6-asan/bin/activate
    pip3 install -r tensorflow-requirements.txt
    ./configure # Choose all default options
    pip3 install -r
    CC=$(which clang) bazel build --config asan --action_env=LD_PRELOAD=$(clang --print-file-name libclang_rt.asan-x86_64.so) --action_env=ASAN_OPTIONS=detect_leaks=0:detect_odr_violation=0 //tensorflow/tools/pip_package:--runs_per_test=10 --cache_test_results=nobuild_pip_package
    CC=$(which clang) bazel build --config asan --action_env=LD_PRELOAD=$(clang --print-file-name libclang_rt.asan-x86_64.so) --action_env=ASAN_OPTIONS=detect_leaks=0:detect_odr_violation=0 //tensorflow/core/kernels:all --runs_per_test=10 --cache_test_results=no # Only if you wish to run the IvySyn fuzzer with ASan
    ./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp
    pip3 install --force-reinstall --no-deps /tmp/tensorflow-2.6.0-cp39-cp39-linux_x86_64.whl

If you wish to install in a separate directory, you can specify the installation path using the option `--output_user_root=/path/to/install` before the `build` keyword, as follows:

    CC=$(which clang) bazel --output_user_root=/path/to/install build --config asan --action_env=LD_PRELOAD=$(clang --print-file-name libclang_rt.asan-x86_64.so) --action_env=ASAN_OPTIONS=detect_leaks=0:detect_odr_violation=0 //tensorflow/tools/pip_package:build_pip_package

If your system is limited on resources, you can append `-j N` at the end of the build command to reduce the number of parallel jobs while building (where `N` is the number of parallel jobs).

> Note: IvySyn runs some developer tests (when running `run_kernel_tests.py`), which are produced when the `... bazel build //tensorflow/core/kernels:all --runs_per_test=10 --cache_test_results=no` command is run). If you don't wish to run the IvySyn fuzzer with ASan and only want the ASan installation for analysis purposes, you can skip this command when building with ASan.

### Running PoVs with ASan

To run a PoV with the ASan-compiled TensorFlow installation, first activate the virtual environment containing the ASan installation, and then run the PoV with the environment variables set as follows:

    source ${IVYSYN_PATH}/venv/tensorflow-2.6-asan/bin/activate
    LD_PRELOAD=$(clang --print-file-name libclang_rt.asan-x86_64.so) ASAN_OPTIONS=detect_leaks=0:symbolize=1:detect_odr_violation=0 python3 pov.py
