#!/usr/bin/env python3

import argparse
import glob
import os
import sys
import time

IVYSYN_PATH = os.getenv("IVYSYN_PATH")
if IVYSYN_PATH is None:
    sys.exit(
        "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!")
if IVYSYN_PATH[-1] != "/":
    IVYSYN_PATH = IVYSYN_PATH + "/"

IVYSYN_TMP_RESULTS_PATH = os.getenv("IVYSYN_TMP_RESULTS_PATH")
if IVYSYN_TMP_RESULTS_PATH is None:
    sys.exit(
        "You need to set the IVYSYN_TMP_RESULTS_PATH environment variable to the directory "
        "you want IvySyn to temporarily output results while fuzzing!")
if IVYSYN_TMP_RESULTS_PATH[-1] != "/":
    IVYSYN_TMP_RESULTS_PATH = IVYSYN_TMP_RESULTS_PATH + "/"


def main():

    done_kernels = glob.glob(IVYSYN_TMP_RESULTS_PATH + "*done")
    with open(IVYSYN_TMP_RESULTS_PATH + "start_time.txt", "r") as f:
        start_time = f.read().strip()
        start_time = float(start_time)

    cur_time = time.clock_gettime(time.CLOCK_MONOTONIC)
    elapsed = int((cur_time - start_time) / 60)

    print(f"Kernels fuzzed so far: {len(done_kernels)}")
    print(f"Time elapsed since start of run: {elapsed} mins")


if __name__ == "__main__":
    main()
