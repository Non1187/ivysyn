__and__
__iand__
__ilshift__
__ior__
__irshift__
__ixor__
__lshift__
__or__
__rshift__
__xor__
_add_batch_dim
_aminmax
_aminmax_all
_assert_async_cpu
_autocast_to_full_precision
_autocast_to_reduced_precision
_batch_norm_impl_index
_batch_norm_impl_index_backward
_bincount_cpu
_cdist_backward
_cdist_forward
_choose_qparams_per_tensor
_coalesce_sparse_cpu
_compute_linear_combination
_conj
_conj_physical
_convolution
_convolution_double_backward
_cufft_clear_plan_cache
_cufft_get_plan_cache_max_size
_cufft_get_plan_cache_size
_cufft_set_plan_cache_max_size
_debug_has_internal_overlap
_dim_arange
_dirichlet_grad_cpu
_embedding_bag_backward
_embedding_bag_cpu
_embedding_bag_dense_backward_cpu
_embedding_bag_forward_only_cpu
_embedding_bag_per_sample_weights_backward_cpu
_embedding_bag_sparse_backward
_euclidean_dist
_fake_quantize_learnable_per_channel_affine
_fake_quantize_learnable_per_channel_affine_backward
_fake_quantize_learnable_per_tensor_affine
_fake_quantize_learnable_per_tensor_affine_backward
_fake_quantize_per_tensor_affine_cachemask_tensor_qparams
_fft_c2c_mkl
_fft_c2r_mkl
_fft_r2c_mkl
_fw_primal
_gather_sparse_backward
_grid_sampler_2d_cpu_fallback
_grid_sampler_2d_cpu_fallback_backward
_has_compatible_shallow_copy_type
_has_same_storage_numel
_indices_sparse
_is_zerotensor
_local_scalar_dense_cpu
_logcumsumexp_cpu
_lu_with_info
_make_dual
_neg_view
_new_zeros_with_same_feature_meta
_nnpack_spatial_convolution
_nnz_sparse
_pack_padded_sequence
_pack_padded_sequence_backward
_pad_packed_sequence
_pdist_backward
_pdist_forward
_remove_batch_dim
_reshape_alias
_reshape_from_tensor
_rowwise_prune
_s_where
_saturate_weight_to_fp16
_shape_as_tensor
_sobol_engine_draw
_sparse_addmm
_sparse_csr_mm
_sparse_log_softmax
_sparse_mm
_sparse_softmax
_sparse_sum
_sparse_sum_backward_cpu
_sspaddmm_out_only_sparse
_standard_gamma_grad_cpu
_test_ambiguous_defaults
_test_optional_floatlist
_test_optional_intlist
_test_serialization_subcmul
_test_warn_in_autograd
_trilinear
_unique2_cpu
_unique_cpu
_unpack_dual
_unsafe_view
_upsample_bicubic2d_aa
_upsample_bicubic2d_aa_backward
_upsample_bilinear2d_aa
_upsample_bilinear2d_aa_backward
_upsample_nearest_exact1d
_upsample_nearest_exact1d_backward
_upsample_nearest_exact2d
_upsample_nearest_exact2d_backward
_upsample_nearest_exact2d_quantized_cpu
_upsample_nearest_exact3d_backward_cpu
_upsample_nearest_exact3d_cpu
_upsample_nearest_exact3d_quantized_cpu
_validate_sparse_csr_tensor_args
_values_sparse
_version
abs
absolute
adaptive_avg_pool1d
adaptive_avg_pool2d
adaptive_avg_pool2d_backward_cpu
adaptive_avg_pool2d_cpu
adaptive_avg_pool2d_quantized_cpu
adaptive_avg_pool3d
adaptive_avg_pool3d_backward_cpu
adaptive_avg_pool3d_cpu
adaptive_avg_pool3d_quantized_cpu
adaptive_max_pool1d
add
add_out_sparse_cpu
add_relu
add_sparse
add_sparse_csr
add_zerotensor
addmm_out_sparse_csr_cpu
addmm_out_sparse_dense_cpu
addmm_sparse_dense_cpu
addmv_out_sparse_csr
addr
adjoint
affine_grid_generator
affine_grid_generator_backward
alias
align_as
allclose
alpha_dropout
amax
amin
angle
any_sparse
arccos
arccosh
arcsin
arcsinh
arctan
arctan2
arctanh
argsort
argwhere
as_strided_qtensorimpl
as_strided_tensorimpl
atleast_1d
atleast_2d
atleast_3d
avg_pool1d
avg_pool2d_quantized_cpu
avg_pool3d_quantized_cpu
batch_norm
batch_norm_backward_cpu
batch_norm_cpu
batch_norm_update_stats_cpu
bilinear
binary_cross_entropy_backward_cpu
binary_cross_entropy_cpu
binary_cross_entropy_with_logits
binary_cross_entropy_with_logits_backward
bitwise_and
bitwise_left_shift
bitwise_or
bitwise_right_shift
bitwise_xor
bmm_sparse_cpu
broadcast_to
bucketize_cpu
can_cast
cdist
celu
channel_shuffle
channel_shuffle_cpu
channel_shuffle_quantized_cpu
cholesky
cholesky_inverse
cholesky_solve
choose_qparams_optimized
chunk
clamp_max
clamp_min
clamp_quantized_cpu
clip
coalesce
col2im_backward_cpu
col2im_cpu
col_indices_sparse_csr
combinations
complex
conj
conj_physical
constant_pad_nd
contiguous
conv1d
conv2d
conv3d
conv_tbc
conv_tbc_backward
conv_transpose1d
conv_transpose2d
conv_transpose3d
convolution
convolution_backward
convolution_backward_overrideable
convolution_overrideable
copysign
corrcoef
cosine_embedding_loss
cosine_similarity
count_nonzero
count_nonzero_cpu
count_nonzero_cuda
cov
cpu_equal
cross
cross_entropy_loss
crow_indices_sparse_csr
ctc_loss
ctc_loss_backward_cpu
ctc_loss_cpu
cudnn_is_acceptable
cummax
cummaxmin_backward
cummin
cumprod_backward
cumulative_trapezoid
data
deg2rad
dense_dim_sparse
dense_to_mkldnn
dense_to_sparse
dequantize_cpu_or_cuda
dequantize_quantized
det
detach
diag
diag_backward
diag_embed
diagflat
diagonal
diagonal_backward
diagonal_scatter
diff
dist
div
div_sparse
div_zerotensor
divide
dot
dropout
dsplit
eig
embedding
embedding_backward
embedding_bag
embedding_dense_backward_cpu
embedding_sparse_backward
equal_quantized_cpu
expand
expand_as
fake_quantize_per_channel_affine
fake_quantize_per_channel_affine_cachemask
fake_quantize_per_channel_affine_cachemask_backward
fake_quantize_per_tensor_affine
fake_quantize_per_tensor_affine_cachemask
fake_quantize_per_tensor_affine_cachemask_backward
fbgemm_linear_fp16_weight
fbgemm_linear_fp16_weight_fp32_activation
fbgemm_linear_int8_weight
fbgemm_linear_int8_weight_fp32_activation
fbgemm_linear_quantize_weight
fbgemm_pack_gemm_matrix_fp16
fbgemm_pack_quantized_matrix
feature_alpha_dropout
feature_dropout
fft_fftshift
fft_ifftshift
fix
flatten
flip
fliplr
flipud
float_power
floor_divide
floor_divide_out_sparse_zerodim
floor_divide_sparse
fmod
fractional_max_pool3d_backward_cpu
frexp
frobenius_norm
fused_moving_avg_obs_fake_quant
fused_moving_avg_obs_fake_quant_cpu
gather_backward
gelu_quantized_cpu
geqrf
ger
glu_backward_cpu
greater
greater_equal
grid_sampler
grid_sampler_2d_cpu
grid_sampler_3d_backward_cpu
grid_sampler_3d_cpu
group_norm
hardshrink
hardsigmoid_quantized_cpu
hardswish
hardswish_backward
hardtanh
hardtanh_backward
hardtanh_quantized_cpu
hinge_embedding_loss
histogram_cpu
histogram_histc_cpu
histogramdd_bin_edges_cpu
hsplit
hspmm_sparse_cpu
huber_loss
huber_loss_backward
im2col_backward_cpu
im2col_cpu
imag
index_copy
index_fill
index_select_backward
index_select_sparse
indices_sparse
infinitely_differentiable_gelu_backward
inner
instance_norm
int_repr_quantized_cpu
inverse
is_coalesced_sparse
is_complex
is_conj
is_distributed
is_floating_point
is_inference
is_leaf
is_neg
is_nonzero
is_same_size
is_set_to
is_signed
isclose
isfinite
isinf
isnan
isreal
istft
item
kl_div
kl_div_backward_cpu
kron
kthvalue
l1_loss
l1_loss_backward
layer_norm
layer_norm_backward_cpu
layer_norm_cpu
ldexp
leaky_relu_quantized_cpu
legacy_lstsq
less
less_equal
linalg_cholesky
linalg_cholesky_ex
linalg_cond
linalg_cross
linalg_det
linalg_diagonal
linalg_eig
linalg_eigvals
linalg_householder_product
linalg_inv
linalg_inv_ex
linalg_lu_factor
linalg_matmul
linalg_matrix_exp
linalg_matrix_norm
linalg_matrix_power
linalg_matrix_rank
linalg_norm
linalg_pinv
linalg_slogdet
linalg_solve
linalg_solve_triangular
linalg_svd
linalg_svdvals
linalg_tensorinv
linalg_tensorsolve
linalg_vector_norm
linear
log_sigmoid
log_sigmoid_backward_cpu
log_sigmoid_backward_cuda
log_sigmoid_forward_cpu
log_softmax
log_softmax_backward_sparse_cpu
log_softmax_sparse_cpu
logcumsumexp
logdet
logical_and
logical_not
logical_or
logical_xor
logit
logsumexp
lu_solve
lu_unpack
mH
mT
make_per_channel_quantized_tensor_cpu
make_per_tensor_quantized_tensor_cpu
margin_ranking_loss
masked_fill
masked_fill__cpu
masked_scatter
masked_scatter__cpu
masked_select_backward
masked_select_cpu
masked_softmax_cpu
math_addr
math_channel_shuffle
math_group_norm
math_mish_backward
math_native_layer_norm
math_silu_backward
matmul
matrix_H
matrix_exp
matrix_exp_backward
matrix_power
matrix_rank
max
max_pool1d
max_pool1d_with_indices
max_pool2d
max_pool3d
max_pool3d_with_indices_backward_cpu
max_pool3d_with_indices_cpu
max_quantized_cpu
max_unpooling2d_backward_cpu
max_unpooling2d_forward_cpu
max_unpooling3d_backward_cpu
max_unpooling3d_forward_cpu
mean
mean_quantized_cpu
median
median_cpu
min
min_quantized_cpu
miopen_batch_norm
miopen_batch_norm_backward
miopen_convolution
miopen_convolution_transpose
miopen_depthwise_convolution
mish_backward
mkldnn_adaptive_avg_pool2d
mkldnn_adaptive_avg_pool2d_backward
mkldnn_add
mkldnn_avg_pool2d
mkldnn_avg_pool2d_backward
mkldnn_avg_pool3d
mkldnn_avg_pool3d_backward
mkldnn_batch_norm
mkldnn_batch_norm_backward
mkldnn_convolution
mkldnn_gelu
mkldnn_gelu_backward
mkldnn_linear
mkldnn_linear_backward
mkldnn_linear_backward_input
mkldnn_linear_backward_weights
mkldnn_max_pool2d
mkldnn_max_pool2d_backward
mkldnn_max_pool3d
mkldnn_max_pool3d_backward
mkldnn_mul
mkldnn_relu
mkldnn_relu_backward
mkldnn_reorder_conv2d_weight
mkldnn_reorder_conv3d_weight
mkldnn_reshape
mkldnn_sigmoid
mkldnn_tanh
mkldnn_to_dense
mkldnn_transpose
mkldnn_view
mode
moveaxis
movedim
mse_loss
mse_loss_backward
msort
mul
mul_out_sparse_cpu
mul_sparse
mul_zerotensor
multi_head_self_attention_cpu
multi_margin_loss_cpu
multi_margin_loss_cpu_backward
multilabel_margin_loss
multilabel_margin_loss_backward_cpu
multilabel_margin_loss_forward_cpu
multiply
mv
mv_sparse
mvlgamma
nan_to_num
nan_to_num_sparse
nanmean
nanmedian
nanmedian_cpu
nansum
narrow
narrow_copy_dense
narrow_copy_dense_cpu
narrow_copy_sparse
native_dropout_backward_cpu
native_dropout_cpu
native_group_norm
native_group_norm_backward
neg_sparse
negative
nll_loss
nll_loss2d
nll_loss2d_backward_cpu
nll_loss2d_forward_cpu
nll_loss_nd
nonzero_cpu
nonzero_numpy
norm
norm_except_dim
norm_sparse
not_equal
nuclear_norm
numpy_T
one_hot
orgqr
ormqr
outer
output_nr
pairwise_distance
pdist
permute
pinverse
pixel_shuffle
pixel_unshuffle
poisson_nll_loss
polar
positive
pow_out_sparse_scalar
pow_sparse_scalar
prelu_backward_cpu
prelu_cpu
prod
promote_types
put
q_per_channel_axis
q_per_channel_scales
q_per_channel_zero_points
q_scale_quant
q_zero_point_quant
qmax
qmin
qr
qscheme_quant
quantize_per_channel
quantize_per_tensor
quantize_per_tensor_dynamic
quantize_per_tensor_tensor_qparams
quantized_batch_norm
quantized_max_pool1d
quantized_max_pool2d
rad2deg
ravel
real
reflection_pad1d_cpu
reflection_pad2d_backward_cpu
reflection_pad2d_cpu
relu
relu6
relu_quantized_cpu
remainder
repeat
repeat_interleave
repeat_interleave_cpu
replication_pad2d_backward_cpu
replication_pad3d_backward_cpu
reshape
reshape_as
resolve_conj
resolve_neg
retain_grad
retains_grad
roll_cpu
rot90
round_sparse_csr
rrelu_with_noise_backward
rsub
select
select_backward
select_scatter
selu
set_data
sigmoid_quantized_cpu
size
slice
slice_backward
slice_scatter
slogdet
slow_conv2d_backward_cpu
slow_conv2d_forward_cpu
slow_conv3d
slow_conv3d_forward_cpu
slow_conv_dilated2d_cpu
slow_conv_dilated3d_cpu
slow_conv_transpose3d_cpu
smm
smooth_l1_loss_backward
soft_margin_loss
soft_margin_loss_backward
softmax
softmax_backward_sparse_cpu
softmax_sparse_cpu
solve
sort_cpu
sort_cpu_stable
sort_quantized_cpu
sort_quantized_cpu_stable
sparse_broadcast_to
sparse_dim_sparse
sparse_dtype_norm
sparse_mask_cpu
sparse_norm
sparse_sparse_matmul_cpu
sparse_to_dense
special_digamma
special_erf
special_erfc
special_erfinv
special_exp2
special_expit
special_expm1
special_gammainc
special_gammaincc
special_gammaln
special_i0
special_log1p
special_log_softmax
special_logit
special_logsumexp
special_multigammaln
special_ndtr
special_polygamma
special_psi
special_round
special_sinc
special_softmax
special_xlog1py
special_xlogy
special_zeta
split
split_with_sizes
square
squeeze
squeeze_quantized
sspaddmm
std
std_mean
stft
stride
sub
sub_sparse
subtract
sum
sum_to_size
svd
swapaxes
swapdims
symeig
t
take
take_along_dim
tanh_quantized_cpu
tensor_split
tensordot
thnn_conv2d
threshold_quantized_cpu
tile
to_dense_backward
to_mkldnn_backward
topk_quantized_cpu
trace_backward
trace_cpu
transpose
trapezoid
trapz
triangular_solve_out_sparse_csr_cpu
triplet_margin_loss
true_divide
type_as
unbind
unfold
unfold_backward
unique_consecutive_cpu
unique_dim_consecutive_cpu
unique_dim_cpu
unsafe_split
unsafe_split_with_sizes
unsqueeze
unsqueeze_quantized
unsqueeze_sparse
upsample_bicubic2d
upsample_bicubic2d_backward
upsample_bilinear2d
upsample_bilinear2d_backward
upsample_bilinear2d_quantized_cpu
upsample_linear1d
upsample_linear1d_backward
upsample_nearest1d
upsample_nearest1d_backward
upsample_nearest2d
upsample_nearest2d_backward
upsample_nearest2d_quantized_cpu
upsample_nearest3d_backward_cpu
upsample_nearest3d_cpu
upsample_nearest3d_quantized_cpu
upsample_trilinear3d
upsample_trilinear3d_backward
value_selecting_reduction_backward
values_sparse
values_sparse_csr
vander
var
var_mean
vdot
view
view_as
view_dtype
vsplit
where
xlogy