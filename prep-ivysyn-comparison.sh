#!/bin/bash
# set -x

# This script prepares an IvySyn instance for comparison with Atheris

if [[ -z "${IVYSYN_PATH}" ]]; then
    echo "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!";
    exit 1
fi

if [[ -z "${IVYSYN_TMP_RESULTS_PATH}" ]]; then
    echo -n "You need to set the IVYSYN_TMP_RESULTS_PATH environment variable to the directory "
    echo "you want IvySyn to temporarily output results while fuzzing!";
    exit 1
fi

IVYSYN_SCRIPTS_BASE="${IVYSYN_PATH}/src/ivysyn/scripts/"
IVYSYN_RESULTS_DIR="${IVYSYN_PATH}/results/"
FRAMEWORKS_BASE="${IVYSYN_PATH}/src/frameworks/"
VENV_BASE="${IVYSYN_PATH}/venv/"
CONDA_ACTIVATE_PATH="${VENV_BASE}anaconda3/bin/activate"

PYTORCH_BASE="${FRAMEWORKS_BASE}pytorch-1.11-ivysyn/"
PYTORCH_KERNELS="${PYTORCH_BASE}/aten/src/ATen/native/"
PT_FILES_PATH="${IVYSYN_PATH}/src/ivysyn/pytorch/"
PT_SCRIPTS_PATH="${PT_FILES_PATH}scripts/"
PT_RESULTS_PATH="${IVYSYN_PATH}/results/pytorch/"

TENSORFLOW_BASE="${FRAMEWORKS_BASE}tensorflow-2.6-ivysyn/"
TENSORFLOW_PYTHON_PATH="${TENSORFLOW_BASE}/tensorflow/python/"
TENSORFLOW_KERNELS="${TENSORFLOW_BASE}tensorflow/core/kernels/"
TF_FILES_PATH="${IVYSYN_PATH}/src/ivysyn/tensorflow/"
TF_SCRIPTS_PATH="${TF_FILES_PATH}scripts/"
TF_RESULTS_PATH="${IVYSYN_PATH}/results/tensorflow/"


bazel_oom_message()
{
    echo "TensorFlow build failed. Your system probably run out of memory while building." >&2
    echo -n "Try reducing the number of parallel jobs used by bazel by changing the value " >&2
    echo -n " of the '-j' argument for the 'bazel build ... ' command in `basename $0`" >&2
    echo " (e.g., bazel build -j 2 ...)." >&2
    echo "If the problem persists, contact the authors." >&2

    exit 1
}

prep_pytorch()
{
    echo "Modyfing PyTorch $fuzzer-comparison kernels"

    pushd "${PYTORCH_KERNELS}"
    rm -v native_functions_no_dups.yaml
    git restore *
    cp ${PT_FILES_PATH}native_functions_no_dups.yaml "${PYTORCH_KERNELS}"
    popd

    source "${CONDA_ACTIVATE_PATH}"
    conda activate pytorch-1.11-ivysyn

    pushd "${PT_SCRIPTS_PATH}"
    python3 inject_fuzzing_code.py --$fuzzer-common > "${PT_RESULTS_PATH}instrumentation/instrumentation_output.txt"
    popd

    pushd "${PYTORCH_BASE}"
    MAX_JOBS=$(($(nproc) / 2)) python3 setup.py install
    popd

    conda deactivate

}

prep_tensorflow()
{
    echo "Modyfing TensorFlow $fuzzer-comparison kernels"

    pushd "${TENSORFLOW_KERNELS}"
    git restore *
    popd
    pushd "${TENSORFLOW_PYTHON_PATH}"
    git restore *
    popd

    pushd "${TF_FILES_PATH}/inject-fuzzer"
    make
    popd

    pushd "${TF_SCRIPTS_PATH}"
    bash inject_fuzzing_code.sh --$fuzzer-common > "${TF_RESULTS_PATH}instrumentation/instrumentation_output.txt"
    popd

    # Re-install framework
    pushd ${TENSORFLOW_BASE}
    source "${VENV_BASE}tensorflow-2.6-ivysyn/bin/activate"
    pip3 install -U pip numpy wheel packaging
    pip3 install -U keras_preprocessing --no-deps
    pip3 install -r ${IVYSYN_PATH}/tensorflow-requirements.txt
    # Run with fewer parallel jobs since container configuration sometimes runs out of memory when compiling
    # This is a conservative estimation and can be increased, but bear in mind that bazel might run out of memory
    # with more parallel processes
    bazel build -j $(($(nproc) / 2)) //tensorflow/tools/pip_package:build_pip_package --runs_per_test=10 --cache_test_results=no
    if [[ ! "$?" -eq 0 ]]; then
        bazel_oom_message
    fi
    bazel build -j $(($(nproc) / 2)) //tensorflow/core/kernels:all --runs_per_test=10 --cache_test_results=no
    if [[ ! "$?" -eq 0 ]]; then
        bazel_oom_message
    fi
    ./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp
    pip3 install --force-reinstall /tmp/tensorflow-2.6.0-cp39-cp39-linux_x86_64.whl
    pip3 install -r ${IVYSYN_PATH}/tensorflow-requirements.txt
    popd

}

main()
{
    PREP_TENSORFLOW=0
    PREP_PYTORCH=0
    POSITIONAL=()
    while [[ $# -gt 0 ]]; do

        key="$1"

        case $key in
        --atheris)
            shift
            fuzzer="atheris"
            ;;
        --docter)
            shift
            fuzzer="docter"
            ;;
        --tensorflow)
            shift
            PREP_TENSORFLOW=1
            ;;
        --pytorch)
            shift
            PREP_PYTORCH=1
            ;;
        *)
            echo "Unknown option: $1"
            exit 1
            ;;
        esac

    done

    set -- "${POSITIONAL[@]}"

    if [[ ! -d "${PYTORCH_BASE}"
        || ! -d "${TENSORFLOW_BASE}"
        || ! -d "${IVYSYN_TMP_RESULTS_PATH}" ]]; then
        echo "You need to run 'prep-ivysyn.sh' first!"
        echo -n "This script only modifies the kernels on the pre-installed frameworks"
        echo " to prepare for the Atheris-comparison experiment"
        echo "'prep-ivysyn.sh' takes care of downloading and installing the frameworks first"
    fi

    if [[ -z $fuzzer ]]; then
        echo "Choose either --atheris or --docter"
        exit 1
    fi

    # Modify the kernels for the atheris experiment and re-install
    if [[ ${PREP_PYTORCH} -eq 1 ]]; then
        prep_pytorch
    fi

    if [[ ${PREP_TENSORFLOW} -eq 1 ]]; then
        prep_tensorflow
    fi
}

main $@
