# IvySyn

`IvySyn` is a fully-automated framework for discovering memory error vulnerabilities in Deep Learning (DL) frameworks.

`IvySyn` automatically instruments the native-level API implementations (i.e., C/C++ kernels) of DL frameworks to inject fuzzing wrappers, and leverages developer-provided tests to bootstrap force-executed fuzzing sessions. More specifically, `IvySyn` performs type-aware, mutation-based fuzzing, using the statically-typed nature of the kernel APIs. `IvySyn` can also use the low-level crashing inputs uncovered during fuzzing to synthesize _Proof of Vulnerability_ (PoV) snippets: that is, Python code which triggers the low-level memory errors (in C/C++ code) through a high-level (e.g., Python) API.

`IvySyn` is described in detail in the following [USENIX Security 2023](https://www.usenix.org/conference/usenixsecurity23) [paper](https://arxiv.org/abs/2209.14921):

```
@inproceedings{ivysyn_sec2023,
	title     = {{IvySyn: Automated Vulnerability Discovery in Deep Learning Frameworks}},
	author    = {Christou, Neophytos and Jin, Di and Atlidakis, Vaggelis and Ray, Baishakhi and Kemerlis, Vasileios P.},
	booktitle = {USENIX Security Symposium (SEC)},
	year      = {2023}
}
```

[[_TOC_]]

# Quick Setup

### Required packages

`IvySyn` has been tested in Debian v11 ([bullseye](https://www.debian.org/releases/bullseye/)) only. It requires the following packages:

    sudo apt install git python3-pip python3-dev llvm-dev clang-11 ripgrep fd-find python3-venv cmake clang-tools-11 libclang-11-dev openjdk-11-jdk java-common bc build-essential clang nasm wget

### Environment variables

You will need to set the following environment variables:

- `IVYSYN_PATH`: needs to be set to the directory where you cloned `IvySyn`
- `IVYSYN_TMP_RESULTS_PATH`: needs to be set to the directory you want `IvySyn` to generate logs while fuzzing (`IvySyn` will create the directory and mount it as `tmpfs`, automatically)

### Bootstrap

We provide a script which bootstraps `IvySyn` by doing the following:
- Mounts the directory that will be used by the `IvySyn` fuzzer to output logs, while fuzzing, as `tmpfs`
- Downloads [PyTorch](https://github.com/pytorch/pytorch) or [TensorFlow](https://github.com/tensorflow/tensorflow) and patches them to make them compatible to be run with IvySyn
- Creates Python virtual environments (required by IvySyn), and installs the required packages
- Builds and installs the two frameworks

To run, execute the following:
```bash
cd ${IVYSYN_PATH}/src/ivysyn/scripts
bash ./prep-ivysyn.sh
```

# Running IvySyn

We provide a script which runs `IvySyn` on either `PyTorch` or `TensorFlow`. This script does the following:
- Changes the seeds that will be fed to the random number generators used by `IvySyn` (i.e., the RNGs that are used by the fuzzer, and an RNG that shuffles the order by which the developer tests are run for each framework)
- Re-compiles the framework that will be fuzzed
- Runs the `IvySyn` fuzzer by running the framework's developer tests and saves the results
- Runs `IvySyn`'s synthesizer to create PoVs from the discovered crashes

The script also supports an optional `--nkernels` argument, which allows the user to specify the number of kernels they wish to fuzz. IvySyn will stop fuzzing after the chosen amount of kernels have been fuzzed.

To run, execute the following:
```bash
cd ${IVYSYN_PATH}/src/ivysyn/scripts
./do-run.sh --seed <RNGSEED> --pytorch|--tensorflow [--nkernels NUM_KERNELS]
```

(Replace RNGSEED with an integer and select either `--pytorch` or `--tensorflow`.)

The script will seed the random number generators used by `IvySyn`, re-compile the framework, and start a fuzzing session. The fuzzer will create logs at `${IVYSYN_TMP_RESULTS_PATH}`.

While the fuzzer is running, you can run the [show-fuzzing-progress.py](show-fuzzing-progress.py) script to get some information regarding the current running fuzzing session (e.g., number of kernels fuzzed so far and time elapsed).

The `do-run.sh` script will also print a summary of the run when it's done, along with instructions about how to manually reproduce the PoVs generated by `IvySyn`.

# CVE List

A number of (previously-unknown) vulnerabilities discovered by `IvySyn` in TensorFlow were assigned CVEs. (The PyTorch developers did not request CVEs for the reported bugs.) Below we provide a list of the TensorFlow security advisories containing the vulnerabilities discovered (automatically) by `IvySyn`, which got assigned a CVE, along with the corresponding CVE entry. The TensorFlow advisories also contain the developers' fix for the corresponding bug(s).

- [TFSA-2022-169](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-169.md),[TFSA-2022-086](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-086.md) ([CVE-2022-35935](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35935))
- [TFSA-2022-149](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-149.md) ([CVE-2022-41907](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-41907))
- [TFSA-2022-148](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-148.md) ([CVE-2022-41886](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-41886))
- [TFSA-2022-147](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-147.md) ([CVE-2022-41885](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-41885))
- [TFSA-2022-131](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-131.md) ([CVE-2022-36005](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-36005))
- [TFSA-2022-130](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-130.md) ([CVE-2022-35990](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35990))
- [TFSA-2022-112](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-112.md) ([CVE-2022-36026](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-36026))
- [TFSA-2022-111](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-111.md) ([CVE-2022-36018](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-36018))
- [TFSA-2022-110](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-110.md) ([CVE-2022-35981](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35981))
- [TFSA-2022-109](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-109.md) ([CVE-2022-35979](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35979))
- [TFSA-2022-108](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-108.md) ([CVE-2022-35974](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35974))
- [TFSA-2022-107](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-107.md) ([CVE-2022-35973](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35973))
- [TFSA-2022-106](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-106.md) ([CVE-2022-36019](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-36019))
- [TFSA-2022-105](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-105.md) ([CVE-2022-35972](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35972))
- [TFSA-2022-104](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-104.md) ([CVE-2022-36017](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-36017))
- [TFSA-2022-103](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-103.md) ([CVE-2022-35971](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35971))
- [TFSA-2022-102](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-102.md) ([CVE-2022-35970](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35970))
- [TFSA-2022-101](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-101.md) ([CVE-2022-35969](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35969))
- [TFSA-2022-100](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-100.md) ([CVE-2022-35968](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35968))
- [TFSA-2022-099](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-099.md) ([CVE-2022-35967](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35967))
- [TFSA-2022-098](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-098.md) ([CVE-2022-35966](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35966))
- [TFSA-2022-097](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-097.md) ([CVE-2022-35965](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35965))
- [TFSA-2022-096](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-096.md) ([CVE-2022-35964](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35964))
- [TFSA-2022-095](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-095.md) ([CVE-2022-35963](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35963))
- [TFSA-2022-094](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-094.md) ([CVE-2022-35959](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35959))
- [TFSA-2022-077](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-077.md) ([CVE-2022-29208](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29208))
- [TFSA-2022-076](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-076.md) ([CVE-2022-29203](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29203))
- [TFSA-2022-075](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-075.md) ([CVE-2022-29201](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29201))
- [TFSA-2022-074](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-074.md) ([CVE-2022-29206](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29206))
- [TFSA-2022-071](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-071.md) ([CVE-2021-41197](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-41197))
- [TFSA-2022-070](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-070.md) ([CVE-2022-29196](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29196))
- [TFSA-2022-069](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-069.md) ([CVE-2022-29200](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29200))
- [TFSA-2022-068](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-068.md) ([CVE-2022-29198](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29198))
- [TFSA-2022-067](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-067.md) ([CVE-2022-29199](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29199))
- [TFSA-2022-066](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-066.md) ([CVE-2022-29197](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29197))
- [TFSA-2022-064](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-064.md) ([CVE-2022-29191](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29191))
- [TFSA-2022-063](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-063.md) ([CVE-2022-29194](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29194))
- [TFSA-2022-062](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-062.md) ([CVE-2022-29192](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29192))
- [TFSA-2022-061](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-061.md) ([CVE-2022-29193](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-29193))
- [TFSA-2022-118](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-118.md) ([CVE-2022-35987](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35987))
- [TFSA-2022-117](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-117.md) ([CVE-2022-35986](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35986))
- [TFSA-2022-116](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-116.md) ([CVE-2022-35985](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35985))
- [TFSA-2022-115](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-115.md) ([CVE-2022-35984](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35984))
- [TFSA-2022-114](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-114.md) ([CVE-2022-35983](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35983))
- [TFSA-2022-113](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-113.md) ([CVE-2022-35982](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-35982))

# Repository Structure and IvySyn Components

Our scripts (e.g., `prep-ivysyn.sh` and `do-run.sh`) abstract away `IvySyn`'s components and the interactions between them, and so you don't need to run/interact-with any of the following scripts/files. However, we provide an overview of the structure of the repository, as well as `IvySyn`'s main components for clarity.

## Repository structure

* [results/](results): `IvySyn` will output experiment results (such as instrumentation results, crashes found during fuzzing, and synthesized PoVs) in this directory.
* [venv/](venv): `IvySyn` will create and Python virtual environments for installing separate versions of `PyTorch` and `TensorFlow` in this directory.
* [src/frameworks/](src/frameworks/): `IvySyn` will clone the `PyTorch` and `TensorFlow` source code from their Git repositories under this directory during setup.
* [src/ivysyn/](src/ivysyn/): This directory contains `IvySyn`'s source code.
  * [scripts](src/ivysyn/scripts/): Contains general scripts for setting-up `IvySyn`, or scripts that are common for both frameworks.
  * [pytorch](src/ivysyn/pytorch/): Contains `PyTorch`-specific scripts and files.
  * [tensorflow](src/ivysyn/tensorflow/): Contains `TensorFlow`-specific scripts and files.

## IvySyn components

The main components of `IvySyn` are the following:

* `src/ivysyn/<framework>/src/inject_fuzzing_code(.sh/.py)`: instruments the kernels for the corresponding framework, in order to inject fuzzing wrappers.
* `src/ivysyn/<framework>/fuzzing(.cpp/.cc/.h)`: the `IvySyn` fuzzer for the corresponding framework.
* `src/ivysyn/<framework>/src/run_kernel_tests.py`: `IvySyn`'s "watchdog" script, which kick-starts and monitors the fuzzing process by running the developer-provided framework tests.
* `src/ivysyn/<framework>/src/synthesizer.py` and `src/ivysyn/<framework>/src/reproducer.sh`: The former attempts to synthesize PoVs using the crashes produced by the fuzzer, while the latter checks which PoVs were successfully synthesized and categorizes them based on crash type.

# Responsible Disclosure

We provide a list of the [TensorFlow](results/found_bugs/tf_pov_kernels.txt) and [PyTorch](results/found_bugs/pt_pov_kernels.txt) kernels for which IvySyn has produced PoVs. If you run IvySyn, and find a PoV in a kernel not listed in these files, report the bugs to the developers of the corresponding framework. For PyTorch, use the ["Bug Report"](https://github.com/pytorch/pytorch/issues/new?assignees=&labels=&template=bug-report.yml) issue. For TensorFlow, follow the instructions at: [https://github.com/tensorflow/tensorflow/blob/master/SECURITY.md](https://github.com/tensorflow/tensorflow/blob/master/SECURITY.md#reporting-vulnerabilities).

# License

This software uses the [BSD License](COPYING).
