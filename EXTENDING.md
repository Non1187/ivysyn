# Extending IvySyn

In the case of PyTorch, additional kernels can be supported as follows.

## Unsupported argument types

IvySyn currently does not support (i.e., does not create mutations for) arguments of the following types:

```
at::TensorList
at::native::ResultTypeState
std::array<bool,2>
c10::optional<Generator>
c10::optional<Layout>
c10::optional<Device>
c10::optional<c10::MemoryFormat>
c10::optional<DimnameList>
optional<c10::MemoryFormat>
c10::string_view
c10::optional<c10::string_view>
at::Dimname
torch::List<c10::optional<Tensor>>
optional<at::DimnameList>
at::DimnameList
optional<c10::Layout>
optional<c10::Device>
c10::MemoryFormat
at::native::vulkan::detail::VulkanTensor
ArrayRef<at::native::vulkan::detail::VulkanTensor>
at::native::vulkan::detail::VImage
at::native::vulkan::detail::VBuffer
at::native::vulkan::Conv2DParams
c10::optional<float>
std::array<bool,4>
at::sparse_csr::SparseCsrTensor
ArrayRef<int64_t>
c10::optional<MemoryFormat>
```

To add support for one of these types, follow these steps:

1. In [fuzzing.h](src/ivysyn/pytorch/fuzzing.h), add the following:
    - A `TorchType` enum and a `map_str_enum` entry for the new type. The map entry should contain the type as a string, as it appears in the source code (e.g., "`TensorList`").
    - A new pool (`std::vector<type>`) that will hold the mutations for the new type (similarly to lines 285&ndash;295).
    - The declaration for the function that will initialize the mutation pool for the type (`initialize_type_pool`, if needed).
    - The declaration for the function that will return the next mutation for the type (`get_next_mut_type()`).
2. In [fuzzing.cpp](src/ivysyn/pytorch/fuzzing.cpp):
    - Create the definitions for the two functions that were added in `fuzzing.h` (`initialize_type_pool()` and `get_next_mut_type()`, similarly to lines 2336&ndash;3016).
    - Add a case in the `switch` statement in `Fuzzer()`, which stores the original argument for the type.
    - In `log_current_mutation()`, add a case in the `switch` statement that logs the argument for the type in case of a crash.
    - In `calculate_total_mutations()`, add a case in the `switch` statement that calculates the pool size for the type (similarly to the rest of the statements in the `switch`).
3. In [the driver injection script](src/ivysyn/pytorch/scripts/inject_fuzzing_code.py), add an entry in `fuzzer_typenames` in the `Parser` class, which maps the name of the type as it appears in the source code to the name of the type as you have defined it in `fuzzer.h`, in the `get_next_mut_type` function (e.g., if you named the function `get_next_mut_mytype`, add an entry which maps the type name to `mytype`).
4. Modify [the synthesizer](src/ivysyn/pytorch/scripts/synthesizer.py) as follows:
    - Add an entry for the type in `parse_arg_type`, which maps the type, as it appears in the crash log (as you have defined it in `log_current_mutation` in `fuzzer.cpp`) to the corresponding PyTorch enum (from `tools/codegen/model.py`).
    - Add a case for the type in `parse_crash_args`, similarly to the rest of the if statements, which creates the corresponding Python-level argument for the logged type.

## No matching declaration found

`IvySyn` may not be able to locate a declaration with a matching name for a [number of kernels](results/pytorch/instrumentation/no_matching_decl.txt) listed in the `native_functions.yaml` file of `PyTorch`. To add support for these APIs, check the `PyTorch` source code to find the proper symbol name for the kernel, and add the proper symbol name in the `function_names.txt` file, which will be automatically produced by `IvySyn` under `src/ivysyn/pytorch` after following the setup steps above. These kernels are usually declared using macros (for example [TORCH_META_FUNC](https://github.com/pytorch/pytorch/blob/master/aten/src/ATen/TensorMeta.h#L32)), which change the name of the symbol in the source code.

## Adding new mutations

`IvySyn` currently supports a set of mutations inspired by an [empirical study](https://gitlab.com/brown-ssl/ivysyn#studied-tensorflow-and-pytorch-bugs) we performed re: past PyTorch and TensorFlow vulnerabilities. However, it can be extended
 to add support for additional mutations. If you wish to add additional mutations for an existing type, follow these steps:

- If the mutation does not need to be dynamically created (e.g., add a new value for integer mutations), add the mutation in the corresponding type pool (i.e, `_mutations` variable) in the `fuzzing.h` file ([TensorFlow](src/ivysyn/tensorflow/fuzzing.h), [PyTorch](src/ivysyn/pytorch/fuzzing.h)).
- If the mutations needs to be dynamically created (e.g., a new tensor mutation) or relies on the original values of the arguments, add the new mutation in the corresponding `initialize_x_pool` function in `fuzzing.cpp` ([TensorFlow](src/ivysyn/tensorflow/fuzzing.cpp), [PyTorch](src/ivysyn/pytorch/fuzzing.cpp)). This will initialize the pool as soon the fuzzer for the corresponding kernel is created.
> Note: In TensorFlow, all arguments are represented as tensors, so the new mutation needs to be added in the corresponding case in the `switch` statement in `initialize_tensor_pools`.
