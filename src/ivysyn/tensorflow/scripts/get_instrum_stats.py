import argparse
import os
import sys

IVYSYN_PATH = os.getenv("IVYSYN_PATH")
if IVYSYN_PATH is None:
    sys.exit(
        "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!")
if IVYSYN_PATH[-1] != "/":
    IVYSYN_PATH = IVYSYN_PATH + "/"

INSTRUM_OUT_PATH = IVYSYN_PATH + "/results/tensorflow/instrumentation/"
INSTRUM_OUTPUT_FILE = os.path.join(
    INSTRUM_OUT_PATH, "instrumentation_output.txt")

skipped_reasons = {}


def get_all_kernels(lines):
    allk_lines = [x for x in lines if x.startswith("INFO: Found Compute")]
    allk_names = [x.split(" ")[-3] for x in allk_lines]
    allk_names = set(allk_names)
    return allk_names


def get_instrumented(lines):
    instrumented_lines = [
        x for x in lines if x.startswith("INFO: Successfully")]
    instrumented_names = [x.split(" ")[-1] for x in instrumented_lines]
    instrumented_names = set(instrumented_names)
    return instrumented_names


def get_skipped(lines, instrumented):
    skipped_lines = [x for x in lines if x.startswith("INFO: Skipping")]
    skipped = {x.split(" ")[2]: x[x.index("(") + 1:][:-1]
               for x in skipped_lines}
    skipped_filtered = {x: skipped[x]
                        for x in skipped if x not in instrumented}

    for reason in skipped_filtered.values():

        if reason not in skipped_reasons:
            skipped_reasons[reason] = 0

        skipped_reasons[reason] += 1

    return skipped_filtered.keys()


def get_device_template(lines):

    dev_template = [x for x in lines if "Device template parameter" in x]
    dev_template_kernels = [x.split(':')[2].replace(' ', '')
                            for x in dev_template]

    return set(dev_template_kernels)


def main():

    args_parser = argparse.ArgumentParser(
        description="Parse and transform Pytorch native files")

    args_parser.add_argument(
        "--gpu", dest="gpu_stats", action="store_true", default=False, help="Get GPU stats"
    )

    args = args_parser.parse_args()

    out_prefix = "gpu_" if args.gpu_stats else "cpu_"

    with open(INSTRUM_OUTPUT_FILE, "r") as f:
        lines = f.read().strip().split("\n")

    all_kernels = get_all_kernels(lines)
    instrumented = get_instrumented(lines)
    skipped = get_skipped(lines, instrumented)
    dev_template_kernels = get_device_template(lines)
    dev_templ_instrumented = {
        x for x in instrumented if x in dev_template_kernels}
    dev_templ_skipped = {x for x in skipped if x in dev_template_kernels}

    print(f"{len(all_kernels)} OpKernel classes with Compute() calls\n")
    # print(all_kernels)
    print(f"{len(instrumented)} Compute() calls instrumented\n")
    print(f"{len(skipped)} skipped. Breakdown:")
    print("\n".join([f"\t{k}: {v}" for k, v in skipped_reasons.items()]))
    print()

    with open(os.path.join(INSTRUM_OUT_PATH, f"{out_prefix}all.txt"), "w") as f:
        f.write('\n'.join(all_kernels))

    with open(os.path.join(INSTRUM_OUT_PATH, f"{out_prefix}instrumented.txt"), "w") as f:
        f.write('\n'.join(instrumented))

    with open(os.path.join(INSTRUM_OUT_PATH, f"{out_prefix}skipped.txt"), "w") as f:
        f.write('\n'.join(skipped))

    # print(f"Total kernels with Device template: {len(dev_template_kernels)}")
    # print(f"Of which were instrumented: {len(dev_templ_instrumented)}")
    # print()

    # total_kernel_impls = len(all_kernels) + len(dev_template_kernels)
    # total_kernels_instr = len(instrumented) + len(dev_templ_instrumented)
    # total_kernels_skipped = len(skipped) + len(dev_templ_skipped)
    # print("Total kernel implementations (including double implementations "
    #       f"for Device templates): {total_kernel_impls}")
    # print(f"Total implementations instrumented: {total_kernels_instr}")
    # print(f"Total implementations skipped: {total_kernels_skipped}")


if __name__ == "__main__":
    main()
