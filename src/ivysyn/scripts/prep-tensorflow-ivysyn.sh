#/bin/bash
#
# This script patches TensorFlow to make it compatible with IvySyn and
# instruments the TensorFlow kernels to inject the IvySyn wrappers
#

if [[ -z "${IVYSYN_PATH}" ]]; then
    echo "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!";
    exit 1
fi

if [[ -z "${IVYSYN_TMP_RESULTS_PATH}" ]]; then
    echo -n "You need to set the IVYSYN_TMP_RESULTS_PATH environment variable to the directory "
    echo "you want IvySyn to temporarily output results while fuzzing!";
    exit 1
fi

IVYSYN_VENV="${IVYSYN_PATH}/venv/tensorflow-2.6-ivysyn/bin/activate"
TENSORFLOW_PATH="${IVYSYN_PATH}/src/frameworks/tensorflow-2.6-ivysyn/"
TF_FILES_PATH="${IVYSYN_PATH}/src/ivysyn/tensorflow/"
PATCHES_PATH="${TF_FILES_PATH}patches/"
SCRIPTS_PATH="${TF_FILES_PATH}scripts/"
PASS_PATH="${TF_FILES_PATH}inject-fuzzer/"
RESULTS_PATH="${IVYSYN_PATH}/results/tensorflow/"

apply_patches()
{
    echo "Patching TensorFlow files..."

    patch "${TENSORFLOW_PATH}tensorflow/c/exported_symbols.lds" "${PATCHES_PATH}c_exported_symbols.lds.patch"
    patch "${TENSORFLOW_PATH}tensorflow/BUILD" "${PATCHES_PATH}tensorflow_BUILD.patch"
    patch "${TENSORFLOW_PATH}tensorflow/core/framework/BUILD" "${PATCHES_PATH}tensorflow_core_framework_BUILD.patch"
    patch "${TENSORFLOW_PATH}tensorflow/python/BUILD" "${PATCHES_PATH}tensorflow_python_BUILD.patch"
    patch "${TENSORFLOW_PATH}tensorflow/tf_exported_symbols.lds" "${PATCHES_PATH}tf_exported_symbols.lds.patch"
    patch "${TENSORFLOW_PATH}tensorflow/tf_version_script.lds" "${PATCHES_PATH}tf_version_script.lds.patch"
    patch "${TENSORFLOW_PATH}tensorflow/core/framework/op_kernel.h" "${PATCHES_PATH}op_kernel.h.patch"
    patch "${TENSORFLOW_PATH}tensorflow/core/framework/op_kernel.cc" "${PATCHES_PATH}op_kernel.cc.patch"

    sed -i "s#results_dir = \"PLACEHOLDER\"#results_dir = \"${IVYSYN_TMP_RESULTS_PATH}\"#" "${TF_FILES_PATH}fuzzing.cc"
    sed -i "s#IVYSYN_PATH = \"PLACEHOLDER\"#IVYSYN_PATH = \"${IVYSYN_PATH}\"#" "${TF_FILES_PATH}inject-fuzzer/lib/InjectFuzzer.cpp"
    sed -i "s#file.open(\"PLACEHOLDER#file.open(\"${IVYSYN_PATH}#" "${TENSORFLOW_PATH}tensorflow/core/framework/op_kernel.cc"

    echo "Patching done"
}

copy_files()
{
    echo "Copying ivysyn files..."
    cp ${TF_FILES_PATH}fuzzing* "${TENSORFLOW_PATH}tensorflow/core/framework"
    echo "Files copied"
}

run_pass()
{
    echo "Compiling and running code injecting pass..."

    pushd ${PASS_PATH}
    cmake .
    make
    popd

    pushd ${SCRIPTS_PATH}
    bash inject_fuzzing_code.sh > "${RESULTS_PATH}instrumentation/instrumentation_output.txt"
    popd

    echo "Pass run"
}

main()
{
    . ${IVYSYN_VENV}
    # Patch and copy necessary files to make it compatible with IvySyn
    apply_patches
    copy_files

    # Run the kernel instrumentation pass to modify the kernels
    run_pass
    deactivate
}

main
