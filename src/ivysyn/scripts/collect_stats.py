import argparse
import glob
import os
import sys

IVYSYN_PATH = os.getenv("IVYSYN_PATH")
if IVYSYN_PATH is None:
    sys.exit(
        "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!")
if IVYSYN_PATH[-1] != "/":
    IVYSYN_PATH = IVYSYN_PATH + "/"

TF_IVYSYN_PATH = os.path.join(IVYSYN_PATH, "src/ivysyn/tensorflow/")
PT_IVYSYN_PATH = os.path.join(IVYSYN_PATH, "src/ivysyn/pytorch/")
TF_RESULTS_PATH = os.path.join(
    IVYSYN_PATH, "results/tensorflow/crashes/")
PT_RESULTS_PATH = os.path.join(
    IVYSYN_PATH, "results/pytorch/crashes/")


def main():

    args_parser = argparse.ArgumentParser()

    arg_group = args_parser.add_mutually_exclusive_group(required=True)
    arg_group.add_argument(
        "--tensorflow", dest="tensorflow", action="store_true", default=False)
    arg_group.add_argument(
        "--pytorch", dest="pytorch", action="store_true", default=False)

    args = args_parser.parse_args()

    all_fuzzed = set()

    if args.tensorflow:
        results_path = TF_RESULTS_PATH
    elif args.pytorch:
        results_path = PT_RESULTS_PATH

    ivysyn_run_results = glob.glob(
        results_path + "run?/") + glob.glob(results_path + "full_run?/")

    for run_dir in ivysyn_run_results:
        done_files = glob.glob(run_dir + "*.done")
        nofuzz_files = glob.glob(run_dir + "*.nofuzz")
        done_kernels = [x.split('/')[-1].replace('.done', '')
                        for x in done_files]
        nofuzz_kernels = [x.split('/')[-1].replace('.nofuzz', '')
                          for x in nofuzz_files]
        all_fuzzed.update([x for x in done_kernels if x not in nofuzz_kernels])

    framework = 'TensorFlow' if args.tensorflow else 'PyTorch'
    print(f"{framework}: Total unique kernels fuzzed: {len(all_fuzzed)}")


if __name__ == "__main__":
    main()
