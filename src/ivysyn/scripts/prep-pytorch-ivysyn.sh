#!/bin/bash
#
# This script patches PyTorch to make it compatible with IvySyn and
# instruments the PyTorch kernels to inject the IvySyn wrappers
#

set -x

if [[ -z "${IVYSYN_PATH}" ]]; then
    echo "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!";
    exit 1
fi

if [[ -z "${IVYSYN_TMP_RESULTS_PATH}" ]]; then
    echo -n "You need to set the IVYSYN_TMP_RESULTS_PATH environment variable to the directory "
    echo "you want IvySyn to temporarily output results while fuzzing!";
    exit 1
fi

CONDA_ACTIVATE_PATH="${IVYSYN_PATH}/venv/anaconda3/bin/activate"
PYTORCH_PATH="${IVYSYN_PATH}/src/frameworks/pytorch-1.11-ivysyn/"
PT_FILES_PATH="${IVYSYN_PATH}/src/ivysyn/pytorch/"
PATCHES_PATH="${PT_FILES_PATH}patches/"
SCRIPTS_PATH="${PT_FILES_PATH}scripts/"
RESULTS_PATH="${IVYSYN_PATH}/results/pytorch/"

apply_patches()
{
    echo "Patching PyTorch files..."

    patch "${PYTORCH_PATH}torch/include/ATen/ATen.h" "${PATCHES_PATH}ATen.h.patch"
    patch "${PYTORCH_PATH}aten/src/ATen/ATen.h" "${PATCHES_PATH}ATen.h.patch"
    patch "${PYTORCH_PATH}tools/autograd/gen_python_functions.py" "${PATCHES_PATH}gen_python_functions.py.patch"

    sed -i "s#results_dir = \"PLACEHOLDER\"#results_dir = \"${IVYSYN_TMP_RESULTS_PATH}\"#" "${PT_FILES_PATH}fuzzing.cpp"

    echo "Patching done"
}

copy_files()
{
    echo "Copying IvySyn files..."

    cp ${PT_FILES_PATH}fuzzing* "${PYTORCH_PATH}aten/src/ATen/core"
    cp ${PT_FILES_PATH}native_functions_no_dups.yaml "${PYTORCH_PATH}aten/src/ATen/native"

    echo "Files copied"
}

run_pass()
{
    echo "Running code injecting pass..."

    source "${CONDA_ACTIVATE_PATH}"
    conda activate pytorch-1.11-ivysyn

    pushd ${SCRIPTS_PATH}
    python3 get_fnames.py
    python3 inject_fuzzing_code.py > ${RESULTS_PATH}instrumentation/instrumentation_output.txt
    popd

    conda deactivate
    echo "Pass run"
}

main()
{
    # Patch and copy necessary files to make it compatible with IvySyn
    apply_patches
    copy_files

    # Run the kernel instrumentation pass to modify the kernels
    run_pass
}

main
