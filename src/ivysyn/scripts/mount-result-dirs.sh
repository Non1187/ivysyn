#!/bin/bash

if [[ -z "${IVYSYN_TMP_RESULTS_PATH}" ]]; then
    echo -n "You need to set the IVYSYN_TMP_RESULTS_PATH environment variable to the directory "
    echo "you want IvySyn to temporarily output results while fuzzing!";
    exit 1
fi

sudo mkdir ${IVYSYN_TMP_RESULTS_PATH}
sudo mount -t tmpfs tmpfs ${IVYSYN_TMP_RESULTS_PATH}
sudo chown -R ${USER}:${USER} ${IVYSYN_TMP_RESULTS_PATH}
