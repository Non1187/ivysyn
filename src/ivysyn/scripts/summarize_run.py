import argparse
import glob
import os
import sys


class colors:
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    ENDC = '\033[0m'
    UNDERLINE = '\033[4m'


IVYSYN_PATH = os.getenv("IVYSYN_PATH")
if IVYSYN_PATH is None:
    sys.exit(
        "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!")
if IVYSYN_PATH[-1] != "/":
    IVYSYN_PATH = IVYSYN_PATH + "/"

TF_IVYSYN_PATH = os.path.join(IVYSYN_PATH, "src/ivysyn/tensorflow/")
TF_ATHERIS_COMMON_PYTHON = os.path.join(
    TF_IVYSYN_PATH, "scripts/atheris_comp_scripts/atheris_common_python.txt")
TF_DOCTER_COMMON_PYTHON = os.path.join(
    TF_IVYSYN_PATH, "scripts/docter_comp_scripts/matched_tf.txt")
TF_OPS_TO_KERNELS_FILE = os.path.join(TF_IVYSYN_PATH + "ops_to_kernels.txt")

PT_IVYSYN_PATH = os.path.join(IVYSYN_PATH, "src/ivysyn/pytorch/")
PT_ATHERIS_COMMON_PYTHON = os.path.join(
    PT_IVYSYN_PATH, "scripts/atheris_comp_scripts/atheris_common_python.txt")
PT_DOCTER_COMMON_PYTHON = os.path.join(
    PT_IVYSYN_PATH, "scripts/docter_comp_scripts/matched_pt.txt")
PT_OPS_TO_KERNELS_FILE = os.path.join(PT_IVYSYN_PATH + "ops_to_kernels.txt")

RESULTS_PATH_BASE = os.path.join(IVYSYN_PATH, "results/")
CONDA_ACTIVATE_PATH = os.path.join(IVYSYN_PATH, "venv/anaconda3/bin/activate")


def log_fuzzed_ops(fuzzed_kernels, framework, fuzzer):

    fuzzed_ops = []
    fuzzed_apis = []

    common_ops_file = TF_ATHERIS_COMMON_PYTHON if framework == "tensorflow" else PT_ATHERIS_COMMON_PYTHON
    with open(common_ops_file, "r") as f:
        ops = f.read().strip().split('\n')

    kernel_mappings = {}

    ops_to_kernels = {}
    ops_to_kernels_file = TF_OPS_TO_KERNELS_FILE if framework == "tensorflow" else PT_OPS_TO_KERNELS_FILE
    with open(ops_to_kernels_file, "r") as f:
        mappings = f.read().strip().split("\n")
        for mapping in mappings:
            op_name, kernels = mapping.split(' ')
            kernels = kernels.split(',')
            if framework == "tensorflow":
                kernels = [x for x in kernels if x !=
                           "NoOp" and "GPU" not in x.upper()]
            ops_to_kernels[op_name] = kernels

    for op in ops:
        kernels = ops_to_kernels[op]
        kernel = kernels[0]
        kernel_mappings[kernel] = op

    for kernel_name in fuzzed_kernels:
        if kernel_name.endswith("BaseOp") or kernel_name.endswith("OpBase"):
            op_name = kernel_mappings[kernel_name.replace("Base", "")]
        else:
            op_name = kernel_mappings[kernel_name]

        fuzzed_ops.append(op_name)

    if fuzzer == "docter":
        docter_mappings = {}
        docter_mapping_file = TF_DOCTER_COMMON_PYTHON if framework == "tensorflow" else PT_DOCTER_COMMON_PYTHON
        with open(docter_mapping_file, "r") as f:
            lines = f.read().strip().split('\n')
            for line in lines:
                ivy, doc = line.split(',')
                docter_mappings[ivy] = doc
        fuzzed_apis = [docter_mappings[x] for x in fuzzed_ops]
        out = os.path.join(IVYSYN_PATH, f"docter_comp_fuzzed_{framework}.txt")
        with open(out, "w") as f:
            f.write("\n".join(fuzzed_apis))
            f.write("\n")
    else:
        out = os.path.join(IVYSYN_PATH, f"atheris_comp_fuzzed_{framework}.txt")
        with open(out, "w") as f:
            f.write("\n".join(fuzzed_ops))
            f.write("\n")


def main():

    args_parser = argparse.ArgumentParser()

    arg_group = args_parser.add_mutually_exclusive_group(required=True)
    arg_group.add_argument(
        "--tensorflow", dest="tensorflow", action="store_true", default=False)
    arg_group.add_argument(
        "--pytorch", dest="pytorch", action="store_true", default=False)

    args_parser.add_argument("--dir", dest="dir")

    args = args_parser.parse_args()

    results_path = RESULTS_PATH_BASE
    results_path += "pytorch/" if args.pytorch else "tensorflow/"
    framework_version = "v1.11" if args.pytorch else "v2.6"
    activate_orig_env = f"source {CONDA_ACTIVATE_PATH}; conda activate pytorch-1.11-orig" if args.pytorch \
        else f"source {IVYSYN_PATH}/venv/tensorflow-2.6-orig/bin/activate"
    crashes_dir = os.path.join(results_path, "crashes/", args.dir + "/")
    pov_dir = os.path.join(results_path, "synthesized/",
                           args.dir + "/", framework_version, "reproducible/")
    accum_pov_dir = os.path.join(results_path, "ivysyn_povs/")

    fuzzed_kernels = glob.glob(crashes_dir + "*done")
    fuzzed_kernels = [x.split('/')[-1].replace('.done', '')
                      for x in fuzzed_kernels]
    pov_kernels = [x.split('/')[-1] for x in glob.glob(pov_dir + "*/*")]

    with open(crashes_dir + "start_time.txt", "r") as f:
        start_time = float(f.read().strip())

    with open(crashes_dir + "end_time.txt", "r") as f:
        end_time = float(f.read().strip())

    elapsed_time = int((end_time - start_time) / 60)

    if "_atheris" in args.dir or "_docter" in args.dir:
        log_fuzzed_ops(
            fuzzed_kernels,
            "pytorch" if args.pytorch else "tensorflow",
            "atheris" if "_atheris" in args.dir else "docter")

    print(colors.BOLD)
    print(f"{colors.PURPLE}{colors.UNDERLINE}Summary of run:{colors.ENDC}")
    print(colors.BOLD)
    print(f"{colors.PURPLE}Fuzzed kernels: {colors.BLUE}{len(fuzzed_kernels)}")
    print(f"{colors.PURPLE}Total PoVs produced: {colors.BLUE}{len(set(pov_kernels))}")
    print(f"{colors.PURPLE}Total time: {colors.BLUE}{elapsed_time} mins")
    print(f"{colors.PURPLE}The fuzzer logs can be found at: {colors.BLUE}{crashes_dir}")
    print(f"{colors.PURPLE}The PoVs for the run can be found at: {colors.BLUE}{pov_dir}")
    print()
    print(f"{colors.PURPLE}The PoVs for all the runs are accumulated at {colors.BLUE}{accum_pov_dir}")
    print(f"{colors.PURPLE}If you want to run a PoV yourself, activate the environment containing the pip-installed version of the framework by running")
    print(f"{colors.YELLOW}{activate_orig_env}")
    print(f"{colors.PURPLE}and run the PoV using {colors.YELLOW}python3 pov.py")
    print(colors.ENDC)


if __name__ == "__main__":
    main()
