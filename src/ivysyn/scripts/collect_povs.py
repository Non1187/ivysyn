import argparse
import glob
import os
import shutil
import sys

IVYSYN_PATH = os.getenv("IVYSYN_PATH")
if IVYSYN_PATH is None:
    sys.exit(
        "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!")
if IVYSYN_PATH[-1] != "/":
    IVYSYN_PATH = IVYSYN_PATH + "/"

RESULTS_PATH_BASE = os.path.join(IVYSYN_PATH, "results/")

subfolders = ["segfault/", "fpe/", "abort/"]


def main():

    args_parser = argparse.ArgumentParser()

    arg_group = args_parser.add_mutually_exclusive_group(required=True)
    arg_group.add_argument(
        "--tensorflow", dest="tensorflow", action="store_true", default=False)
    arg_group.add_argument(
        "--pytorch", dest="pytorch", action="store_true", default=False)

    args_parser.add_argument("--dir", dest="dir")

    args = args_parser.parse_args()

    results_path = RESULTS_PATH_BASE
    results_path += "pytorch/" if args.pytorch else "tensorflow/"
    framework_version = "v1.11" if args.pytorch else "v2.6"
    dest_all = results_path + "ivysyn_povs/all/"

    ivysyn_run_results = glob.glob(os.path.join(
        results_path, "synthesized/", args.dir))

    for folder in subfolders:
        dest = results_path + "ivysyn_povs/" + folder
        if not os.path.exists(dest):
            os.makedirs(dest)
    if not os.path.exists(dest_all):
        os.makedirs(dest_all)

    for subfolder in subfolders:
        dest = os.path.join(results_path, "ivysyn_povs/", subfolder)
        src = os.path.join(results_path, "synthesized/",
                           args.dir, framework_version,  "reproducible/", subfolder)
        for crashed_kernel in os.listdir(src):
            kern_dest = dest + crashed_kernel
            full_dest = dest_all + crashed_kernel
            if not os.path.isdir(kern_dest):
                os.makedirs(kern_dest)
            if not os.path.isdir(full_dest):
                os.makedirs(full_dest)
            for filename in glob.glob(src + crashed_kernel + "/*.py"):
                dst_filename = filename.split('/')[-1]
                shutil.copyfile(filename, kern_dest + "/" + dst_filename)
                shutil.copyfile(filename, full_dest + "/" + dst_filename)


if __name__ == "__main__":
    main()
