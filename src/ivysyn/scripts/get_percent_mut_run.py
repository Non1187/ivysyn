import argparse
import glob
import os
import sys

IVYSYN_PATH = os.getenv("IVYSYN_PATH")
if IVYSYN_PATH is None:
    sys.exit(
        "You need to set the IVYSYN_PATH environment variable to the root directory of IvySyn!")
if IVYSYN_PATH[-1] != "/":
    IVYSYN_PATH = IVYSYN_PATH + "/"

TF_IVYSYN_PATH = os.path.join(IVYSYN_PATH, "src/ivysyn/tensorflow/")
PT_IVYSYN_PATH = os.path.join(IVYSYN_PATH, "src/ivysyn/pytorch/")
TF_RESULT_DIR_BASE = os.path.join(IVYSYN_PATH, "results/tensorflow/crashes/")
PT_RESULT_DIR_BASE = os.path.join(IVYSYN_PATH, "results/pytorch/crashes/")


def get_percent_mut_run_for_run(run_folder):

    with open(os.path.join(run_folder, "totals.txt"), "r") as f:
        totals = f.read().strip().split('\n')

    total_mutations = {}
    for line in totals:
        kernel, num = line.split(':')
        total_mutations[kernel] = int(num) + 1

    mutations_run = {}

    mutfiles = glob.glob(run_folder + "*.time.*")
    mutfiles += glob.glob(run_folder + "*.failed.*")

    for filename in mutfiles:

        kernel_name = filename.split('/')[-1].split('.')[0]

        if kernel_name not in mutations_run:
            mutations_run[kernel_name] = 0

        nlines = sum(1 for line in open(filename))
        mutations_run[kernel_name] += nlines

    mutations_run = {k: v for k, v in sorted(
        mutations_run.items(), key=lambda item: item[1])}

    percent_run = {}

    for kernel, nmut_run in mutations_run.items():
        if nmut_run == 0:
            percent_run[kernel] = 0
        else:
            percent_run[kernel] = nmut_run / total_mutations[kernel] * 100

    percent_run = {k: v for k, v in sorted(
        percent_run.items(), key=lambda item: item[1])}

    crashed = glob.glob(run_folder + "*.crash_found")
    crashed = [x.split('/')[-1].replace('.crash_found', '') for x in crashed]

    percent_run_nocrash = {k: v for k,
                           v in percent_run.items() if k not in crashed}

    for kernel, percent in percent_run_nocrash.items():
        print(kernel, percent)


def main():

    args_parser = argparse.ArgumentParser()

    arg_group = args_parser.add_mutually_exclusive_group(required=True)
    arg_group.add_argument(
        "--tensorflow", dest="tensorflow", action="store_true", default=False)
    arg_group.add_argument(
        "--pytorch", dest="pytorch", action="store_true", default=False)

    args_parser.add_argument(
        "--dir", dest="dir", required=True)

    args = args_parser.parse_args()

    if args.tensorflow:
        framework = 'TensorFlow'
        res_folder = TF_RESULT_DIR_BASE + args.dir + "/"
    elif args.pytorch:
        framework = 'PyTorch'
        res_folder = PT_RESULT_DIR_BASE + args.dir + "/"

    print(f"Framework: {framework}")
    get_percent_mut_run_for_run(res_folder)


if __name__ == "__main__":
    main()
