import argparse
import os

cur_path = os.path.dirname(os.path.realpath(__file__))
IVYSYN_PATH = os.path.abspath(os.path.join(cur_path, "../../../")) + "/"


class colors:
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    ENDC = '\033[0m'
    UNDERLINE = '\033[4m'


def main():

    args_parser = argparse.ArgumentParser()

    framework_group = args_parser.add_mutually_exclusive_group(required=True)
    framework_group.add_argument(
        "--tensorflow", dest="tensorflow", action="store_true", default=False)
    framework_group.add_argument(
        "--pytorch", dest="pytorch", action="store_true", default=False)

    fuzzer_group = args_parser.add_mutually_exclusive_group(required=True)
    fuzzer_group.add_argument(
        "--atheris", dest="atheris", action="store_true", default=False)
    fuzzer_group.add_argument(
        "--docter", dest="docter", action="store_true", default=False)

    args = args_parser.parse_args()

    framework = "tensorflow" if args.tensorflow else "pytorch"
    fuzzer = "atheris" if args.atheris else "docter"

    comparison_csv = IVYSYN_PATH + \
        f"comparisons/{fuzzer}_comp_fuzzed_{framework}.csv"

    if args.atheris:
        comparison_results_dir = "/home/ivyuser/ivysyn-atheris/fuzzer_output"
    else:
        comparison_results_dir = f"/home/workdir/{framework}"

    # ivysyn_results_dir = IVYSYN_PATH + \
    #     "results/" + framework + f"/{fuzzer}_comp/"
    ivysyn_results_dir = os.path.join(IVYSYN_PATH,
                                      "results/" + framework + f"/{fuzzer}_comp/")

    with open(ivysyn_results_dir + "total_time.txt", "r") as f:
        total_time = int(f.read().strip())

    with open(ivysyn_results_dir + "fuzzer_logs_dir.txt", "r") as f:
        fuzzer_logs_dir = f.read().strip()

    with open(ivysyn_results_dir + "results.csv", "r") as f:
        entries = f.read().strip().split("\n")

    total_fuzzed_kernels = len(entries)
    results = {}
    total_found = 0
    for line in entries:
        api, start, end, found = line.replace(' ', '').split(',')
        found = found == 'True' or found == '1'
        results[api] = found
        if found:
            total_found += 1

    result_type = "crashes" if args.atheris else "PoVs"
    print()
    print(colors.BOLD)
    print(f"{colors.PURPLE}{colors.UNDERLINE}Results for {colors.YELLOW}IvySyn", end=" ")
    print(f"{colors.PURPLE}on {colors.YELLOW}{framework}{colors.PURPLE}:")
    print(colors.ENDC)
    print(colors.BOLD)
    print(f"{colors.PURPLE}Total kernels fuzzed: {colors.BLUE}{total_fuzzed_kernels}")
    print(f"{colors.PURPLE}Total time of run: {colors.BLUE}{total_time} {colors.PURPLE}mins")
    print(f"{colors.PURPLE}Total {result_type} found: {colors.BLUE}{total_found}")
    print(f"{colors.PURPLE}You can find a CSV in the format \"api_name, api_fuzz_start_time, api_fuzz_end_time, found_crash/found_PoV\" summarizing the run at:")
    print(f"{colors.YELLOW}{ivysyn_results_dir + 'results.csv'}")
    print(f"{colors.PURPLE}You can find the fuzzer logs for this run in the following directory in the IvySyn container:")
    print(f"{colors.YELLOW}{fuzzer_logs_dir}")

    if args.atheris:
        print(f"{colors.PURPLE}IvySyn logs crashing inputs in files ending in _crashes.log inside the above directory")

    if args.docter:
        with open(ivysyn_results_dir + "povs_dir.txt", "r") as f:
            povs_dir = f.read().strip()
        print(f"{colors.PURPLE}You can find the PoVs for this run in the following directory in the IvySyn container:")
        print(f"{colors.YELLOW}{povs_dir}")

    with open(comparison_csv, "r") as f:
        entries = f.read().strip().split("\n")

    total_fuzzed_apis = len(entries)
    start_times = []
    end_times = []
    results = {}
    total_found = 0
    for line in entries:
        api, start, end, found = line.replace(' ', '').split(',')
        found = found == 'True' or found == '1'
        start_times.append(int(float(start)))
        end_times.append(int(float(end)))
        results[api] = found
        if found:
            total_found += 1
    run_start = min(start_times)
    run_end = max(end_times)
    total_time = (run_end - run_start) / 60

    print()
    print(colors.BOLD)
    print(f"{colors.PURPLE}{colors.UNDERLINE}Results for {colors.YELLOW}{fuzzer} {colors.PURPLE}on {colors.YELLOW}{framework}:")
    print(colors.ENDC)
    print(colors.BOLD)
    print(f"{colors.PURPLE}Total APIs fuzzed: {colors.BLUE}{total_fuzzed_apis}")
    print(f"{colors.PURPLE}Total time of run: {colors.BLUE}{total_time} {colors.PURPLE}mins")
    print(f"{colors.PURPLE}Total {result_type} found: {colors.BLUE}{total_found}")
    print(f"{colors.PURPLE}You can find a CSV in the format \"api_name, api_fuzz_start_time, api_fuzz_end_time, found_crash/found_PoV\" summarizing the run at:")
    print(f"{colors.YELLOW}{comparison_csv}")
    print(
        f"{colors.PURPLE}You can find the fuzzer logs for this run in the following directory in the {fuzzer} container:")
    print(f"{colors.YELLOW}{comparison_results_dir}")
    print(colors.ENDC)


if __name__ == "__main__":
    main()
