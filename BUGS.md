# Studied TensorFlow and PyTorch Bugs

We studied ≈240 CVEs assigned to [TensorFlow vulnerabilities](https://github.com/tensorflow/tensorflow/security/advisories), as well as bug reports that [involve memory safety issues in PyTorch](https://github.com/pytorch/pytorch/issues?q=is%3Aissue+label%3A%22module%3A+crash%22+), and put together a set of mutations for input types relevant to DL operations. Below, we provide a representative list of such vulnerabilities that served as inspiration for IvySyn's mutations.

## TensorFlow:

- Large positive values:
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-019.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-007.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-004.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-137.md
- Large negative values:
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-171.md
- Deep set of dimensions:
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-018.md
- Multiple dimensions:
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-005.md
- Empty arguments:
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2022-016.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-140.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-132.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-025.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-026.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-023.md
- Zero values:
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-186.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-176.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-017.md
- Large strings:
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-172.md
- Zero-sized dimensions:
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-148.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-142.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-032.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-020.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-147.md
    - https://github.com/tensorflow/tensorflow/blob/master/tensorflow/security/advisory/tfsa-2021-034.md

## PyTorch:

- Zero dimensions:
    - https://github.com/pytorch/pytorch/issues/73165
- Large positive/negative values:
    - https://github.com/pytorch/pytorch/issues/50960
    - https://github.com/pytorch/pytorch/issues/51134
    - https://github.com/pytorch/pytorch/issues/49521
    - https://github.com/pytorch/pytorch/issues/50961
- Zero values:
    - https://github.com/pytorch/pytorch/issues/41768
    - https://github.com/pytorch/pytorch/issues/41767
- Empty lists:
    - https://github.com/pytorch/pytorch/issues/50967
    - https://github.com/pytorch/pytorch/issues/42541
- Empty tensors:
    - https://github.com/pytorch/pytorch/issues/61656
- Deep tensors:
    - https://github.com/pytorch/pytorch/issues/42616

